# Differential drive robot powered by ROS

All documentation resides in project wiki

## Overview

### 1. Arduino

The Arduino Mega is running a rosserial node with the functionalities:
* 5 ultrasonic sensors input data, publish on ros topic
* 1 IR sensor input data, publish on ros topic
* 1 servo motor output, subscribe to ros topic
* Input from 9 DoF IMU, publish on ros topic
* 12V DC motor control, subscribe to ros topic
* Reading and publising motor encoder values, publish on ros topic

### 2. Raspi

ROS Master node to handle route planning, goal oriented behavior, SLAM, behavior logic, audio, etc.





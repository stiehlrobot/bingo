#include "Arduino.h"

/*This code is for SLAVE in master-slave setup for two arduinos communicating via RS485 modules.
Serial2 is used for communication (read, write) between two RS485 modules
Serial is used to transmit data from slave side RS485 module to slave-pc for debugging and msg review
*/

boolean flag = false;
#define RS485Transmit HIGH
#define RS485Receive  LOW
#define RS485inout  10 // RS485 Transmit or Receive status
#define RS485inout2  11 // RS485 Transmit or Receive status
//#define ledPin      13
#define baudRate    9600

// The data bytes we're sending or receiving
byte rxValue;
byte txValue;


byte srcadd  = 0x01;
byte destadd = 0x05;
byte lenght[2];
int pl_size;
byte buff[8];
byte synch1  = 0x48;
byte synch2  = 0x45;
byte reci;
byte pack[3] = {0x00, 0x00, 0x00};


void setup()
{
  Serial.begin(9600);

  pinMode(RS485inout, OUTPUT);
  pinMode(RS485inout2, OUTPUT);

  // Set RS485 device to read initially

  digitalWrite(RS485inout, RS485Receive);
  digitalWrite(RS485inout2, RS485Receive);
  Serial2.begin(baudRate);

}

void loop()
{
  
  digitalWrite(RS485inout, RS485Receive);
  digitalWrite(RS485inout2, RS485Receive);
  int i = 0;
  delay(100);
  // Is there something on the serial pin?
  while(Serial2.available()>0)
  {
    
    delay(10);
  
    pack[i] = Serial2.read();
    Serial.print("Recieved: 0x");Serial.println(pack[i], HEX);
    flag = true;
    i++;

  }

  digitalWrite(RS485inout, RS485Transmit);
  digitalWrite(RS485inout2, RS485Transmit);
  delay(10);

  if (flag == true){
    for(int u = 0; u < 3;u++){
      Serial.print("Transmitting: 0x");Serial.println(pack[u], HEX);
      Serial2.write(pack[u]);
      delay(1);
    }
  }
  flag = false;
}

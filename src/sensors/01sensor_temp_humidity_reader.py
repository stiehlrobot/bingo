#!/usr/bin/env python


import rospy
from std_msgs.msg import Float32

def handle_temperature(req):
    temp = req.data
    rospy.loginfo("Temperature is: "+ str(temp))

def handle_humidity(req):
    hum = req.data
    rospy.loginfo("Humidity is: "+ str(hum))


if __name__ == '__main__':

    rospy.init_node('temp_hum_reader')
    sub_temp = rospy.Subscriber('temperature', Float32, handle_temperature)
    sub_hum = rospy.Subscriber('humidity', Float32, handle_humidity)

    rospy.spin()